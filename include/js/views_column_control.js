(function ($) {
  /**
   * Handles the setup of the UI for showing and hiding the views control form.
   */
  Drupal.behaviors.views_column_control_column_setup = {
    attach: function (context, settings) {
      $.views_column_control_setup_widget({
        'context': context,
        'el': '#views-column-control-column-wrapper',
        'title': 'Adjust Columns',
        'prepend': '.view .view-content'
      });
    }
  };

  Drupal.behaviors.views_column_control_filter_setup = {
    attach: function (context, settings) {
      $.views_column_control_setup_widget({
        'context': context,
        'el': '#views-column-control-filter-wrapper',
        'title': 'Adjust Filters',
        'prepend': '#views-column-control-filter-form-wrapper'
      });
    }
  };

  $.views_column_control_setup_widget = function(opts) {
    var $el = $(opts.el, opts.context);
    if (!$el.length) {
      return true;
    }

    var $control =
      $('<div>')
        .addClass('control')
        .html('')
        .attr('title', opts.title)
        .click(function() {
          $el.animate({
            width: "toggle",
            height: "toggle",
            opacity: "toggle"
          });
        });

    $('<div>')
      .addClass('views-column-control-widget')
      .append($control)
      .append($el)
      .prependTo(opts.prepend);

    $(document).mouseup(function(e) {
      if (
        $el.is(":visible")
        && !$el.is(e.target)
        && !$control.is(e.target)
        && $el.has(e.target).length === 0
      ) {
        $el.animate({
          width: "toggle",
          height: "toggle",
          opacity: "toggle"
        });
      }
    });

    // Add select and deselect links
    $('<div>')
      .addClass('views-column-control-selections')
      .append(
        $('<a>')
          .attr('href', '#')
          .addClass('views-column-control-select-all')
          .html('Select All')
          .click(function(e) {
            e.preventDefault();
            $.views_column_control_select($el, true);
          })
      )
      .append(
        $('<a>')
          .attr('href', '#')
          .addClass('views-column-control-select-none')
          .html('Select None')
          .click(function(e) {
            e.preventDefault();
            $.views_column_control_select($el, false);
          })
      )
      .appendTo(opts.el);
  };

  /**
   * (De)select all the checkboxes within the given element.
   *
   * @param $el
   * @param select
   */
  $.views_column_control_select = function($el, select) {
    $el.find(':checkbox').prop('checked', select);
  };
})(jQuery);