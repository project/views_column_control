<?php


/**
 * Implements hook_views_plugins().
 */
function views_column_control_views_plugins() {
  $path = drupal_get_path('module', 'views_column_control') . '/include/views';
  $plugins = [];

  $plugins['display_extender'] = [
    'nn_solr' => [
      'title' => t('Change Columns'),
      'help' => t('Allow user the ability to manipulate columns.'),
      'path' => $path,
      'handler' => 'views_column_control_plugin_change_columns',
    ],
  ];

  return $plugins;
}