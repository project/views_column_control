<?php

class views_column_control_plugin_change_columns extends views_plugin_display_extender {
  function add_signature(&$view) {}

  /**
   * Set default values for the form.
   */
  function options_definition(&$options) {
    $options = parent::option_definition();
    $options['views_column_control_columns_enabled'] = [
      'default' => 0,
    ];
    $options['views_column_control_filters_enabled'] = [
      'default' => 0,
    ];
    $options['views_column_control_filters_visible'] = [
      'default' => [],
    ];
    return $options;
  }

  /**
   * Set default values for the form.
   */
  function options_definition_alter(&$options) {
    $options['views_column_control_columns_enabled'] = [
      'default' => 0,
    ];
    $options['views_column_control_filters_enabled'] = [
      'default' => 0,
    ];
    $options['views_column_control_filters_visible'] = [
      'default' => [],
    ];
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ($form_state['section'] === 'views_column_control') {
      $form['views_column_control_columns_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('User Column Control'),
        '#description' => t('User can select which columns to display.'),
        '#default_value' => $this->display->get_option('views_column_control_columns_enabled'),
      );
      $form['views_column_control_filters_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('User Filter Control'),
        '#description' => t('User can select which filters to display.'),
        '#default_value' => $this->display->get_option('views_column_control_filters_enabled'),
      );

      $form_state['view']->pre_execute();

      $options = views_column_control_get_default_visible_filters($form_state['view'], $filters);

      $form['views_column_control_filters_visible'] = array(
        '#type' => 'checkboxes',
        '#title' => t('User Filter\'s Visible'),
        '#description' => t('Select which filters are initially visible to to the user.'),
        '#options' => $filters,
        '#default_value' => $options,
        '#states' => [
          'invisible' => [
            ':input[name="views_column_control_filters_enabled"]' => array('checked' => FALSE),
          ]
        ],
      );
    }
  }

  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) {
    $this->display->set_option(
      'views_column_control_columns_enabled',
      (
        isset($form_state['values']['views_column_control_columns_enabled'])
          ? $form_state['values']['views_column_control_columns_enabled']
          : $this->display->get_option('views_column_control_columns_enabled')
      )
    );
    $this->display->set_option(
      'views_column_control_filters_enabled',
      (
        isset($form_state['values']['views_column_control_filters_enabled'])
          ? $form_state['values']['views_column_control_filters_enabled']
          : $this->display->get_option('views_column_control_filters_enabled')
      )
    );
    $this->display->set_option(
      'views_column_control_filters_visible',
      (
        isset($form_state['values']['views_column_control_filters_visible'])
          ? $form_state['values']['views_column_control_filters_visible']
          : $this->display->get_option('views_column_control_filters_visible')
      )
    );
  }

  /**
   * Provide the default summary for options in the views UI.
   */
  function options_summary(&$categories, &$options) {
    $output = [];
    if ($this->display->get_option('views_column_control_columns_enabled')) {
      $output[] = t('Columns');
    }
    if ($this->display->get_option('views_column_control_filters_enabled')) {
      $output[] = t(
        'Filters (!count/!total)',
          [
            '!count' => count(array_filter($this->display->get_option('views_column_control_filters_visible'))),
            '!total' => count($this->display->get_option('views_column_control_filters_visible')),
          ]
      );
    }
    $ouput = (empty($output) ? 'None' : implode(', ', $output));

    $options['views_column_control'] = array(
      'category' => 'other',
      'title' => t('Views Column Control'),
      'value' => $ouput,
      'desc' => t('User can select which columns to display.'),
    );
  }
}