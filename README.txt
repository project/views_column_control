INSTALLATION
------------

 * Enable required modules
   - Views:  http://drupal.org/project/views
 * Enable Views Column Control module
 * Go to the admin edit page of a new or existing view
 * Under Advanced -> Other, open the User Column Control settings
 * Enable "User Column Control" or "User Filter Control"
 * For columns:
   - initial visibility is controlled by "Exclude from display" setting
   - refining the visible columns can be achieved programmatically by hooks
 * For filters:
   - initial visibility is controlled by "User Filter's Visible" setting
   - only filters which are "exposed to visitors" are available to show/hide
   - refining the visible filters can be achieved programmatically by hooks


PROGRAMMATICALLY SHOWING/HIDING AVAILABLE COLUMNS AND FILTERS
-------------------------------------------------------------

The column and filter selection UIs are normal drupal forms, so a form alter
hook can be used to alter them at need. At implementation might look something
like:

```
function MY_MODULE_form_alter(&$form, &$form_state, $form_id) {
  if ('views_column_control_select_column_form' == $form_id) {
    // don't allow user to alter position or visibility of custom_link fields
    $view = $form_state['build_info']['args'][0];
    foreach ($form['vcc_fields'] as $field_id => $field) {
      if (
        isset($view->field[$field_id]->real_field)
        && 'custom_link' == $view->field[$field_id]->real_field
      ) {
        unset($form['vcc_fields'][$field_id]);
      }
    }
  }
}
```

PROGRAMMATICALLY SHOWING/HIDING DISPLAYED COLUMNS AND FILTERS
-------------------------------------------------------------

The `hook_views_column_control_display_fields_alter` provides the means to
control which fields or filters are displayed on a view. This makes it easy to
always show or a hide columns or filters based on the view, or user permissions,
or other custom criteria at need. Two parameters are passed to this alter:
* $fields (array)
  - An array of booleans keyed by field names, indicating which columns or
    filters should be displayed or hidden.
  - This variable should be recieved by reference and its booleans modified at
    need.

* $context (array)
  - An array of contextual information regarding the set of fields or filters.
  - Keys include:
      'view' => {the view object},
      'export_view' => {boolean indicating if display is of type views_data_export},
      'target_display' => {name of the view's current display, or the display that the export is attached to},
      'type' => {string set to 'column' or 'filter', indicating which context is being altered for this view},

At implementation might look something like:

```
function MY_MODULE_views_column_control_display_fields_alter(&$fields, &$context) {
  if ('column' == $context['type']) {
    if ($context['export_view']) {
      foreach ($fields as $field_id => $display) {
        // On export, hide the specialty link fields, and hide any fields which
        // are altered, since these are fields where the data is collected and
        // formatted with HTML, and doesn't work well in a CSV file.
        if ('custom_link' == $context['view']->field[$field_id]->real_field) {
          $fields[$field_id] = FALSE;
        }
        else if (
          !!$context['view']->field[$field_id]->options['alter']['alter_text']
          && !!$context['view']->field[$field_id]->options['alter']['text']
        ) {
          // Phone number and email fields should be shown, but without special
          // labels (if they're turned off in the admin interface)
          if (
            (FALSE !== strstr($field_id, 'phone_number_'))
            || (FALSE !== strstr($field_id, 'email_address_'))
          ) {
            $context['view']->field[$field_id]->options['alter']['alter_text'] = FALSE;
          }
          else {
            $fields[$field_id] = FALSE;
          }
        }
      }
    }
    else {
      foreach ($fields as $field_id => $display) {
        if ('custom_link' == $context['view']->field[$field_id]->real_field) {
          // always show the custom link field unless we're exporting
          $fields[$field_id] = !$context['export_view'];
        }
      }
    }
  }
}
```